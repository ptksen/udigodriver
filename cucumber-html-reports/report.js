$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("alogin.feature");
formatter.feature({
  "line": 2,
  "name": "In oder to the Open Home page successfully",
  "description": "",
  "id": "in-oder-to-the-open-home-page-successfully",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@ResetWeb"
    },
    {
      "line": 1,
      "name": "@Alogin"
    }
  ]
});
formatter.scenarioOutline({
  "line": 4,
  "name": "Open the Home page successfully",
  "description": "",
  "id": "in-oder-to-the-open-home-page-successfully;open-the-home-page-successfully",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "I open the Home page page \u003cwebsite\u003e",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I verify the login screen in udigo",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "I type email \u003cemail\u003e , password \u003cpassword\u003e",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "I check i\u0027m not a robot",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I click the login button",
  "keyword": "Then "
});
formatter.examples({
  "line": 11,
  "name": "",
  "description": "",
  "id": "in-oder-to-the-open-home-page-successfully;open-the-home-page-successfully;",
  "rows": [
    {
      "cells": [
        "website",
        "email",
        "password"
      ],
      "line": 12,
      "id": "in-oder-to-the-open-home-page-successfully;open-the-home-page-successfully;;1"
    },
    {
      "cells": [
        "https://admin-test.witzmobility.com/home",
        "superAdmin@taxiApp.com",
        "Password"
      ],
      "line": 13,
      "id": "in-oder-to-the-open-home-page-successfully;open-the-home-page-successfully;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 310100,
  "status": "passed"
});
formatter.scenario({
  "line": 13,
  "name": "Open the Home page successfully",
  "description": "",
  "id": "in-oder-to-the-open-home-page-successfully;open-the-home-page-successfully;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@ResetWeb"
    },
    {
      "line": 1,
      "name": "@Alogin"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I open the Home page page https://admin-test.witzmobility.com/home",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I verify the login screen in udigo",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "I type email superAdmin@taxiApp.com , password Password",
  "matchedColumns": [
    1,
    2
  ],
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "I check i\u0027m not a robot",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "I click the login button",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "https://admin-test.witzmobility.com/home",
      "offset": 26
    }
  ],
  "location": "LoginStep.i_open_the_home_page_page(String)"
});
formatter.result({
  "duration": 7850981700,
  "error_message": "org.openqa.selenium.WebDriverException: Reached error page: about:neterror?e\u003ddnsNotFound\u0026u\u003dhttps%3A//admin-test.witzmobility.com/home\u0026c\u003dUTF-8\u0026f\u003dregular\u0026d\u003dWe%20can%E2%80%99t%20connect%20to%20the%20server%20at%20admin-test.witzmobility.com.\nBuild info: version: \u00273.12.0\u0027, revision: \u00277c6e0b3\u0027, time: \u00272018-05-08T14:04:26.12Z\u0027\nSystem info: host: \u0027DESKTOP-2RQEVQT\u0027, ip: \u0027169.254.248.116\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_181\u0027\nDriver info: org.openqa.selenium.firefox.FirefoxDriver\nCapabilities {acceptInsecureCerts: true, browserName: firefox, browserVersion: 62.0.3, javascriptEnabled: true, moz:accessibilityChecks: false, moz:headless: false, moz:processID: 6692, moz:profile: C:\\Users\\Cao Nguyen\\AppData..., moz:useNonSpecCompliantPointerOrigin: false, moz:webdriverClick: true, pageLoadStrategy: normal, platform: XP, platformName: XP, platformVersion: 10.0, rotatable: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}}\nSession ID: 204afc15-e750-496b-8d94-83f2591937dc\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:187)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:122)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:543)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.get(RemoteWebDriver.java:271)\r\n\tat udigodriver.screens.login.LoginScreen.openWebsite(LoginScreen.java:34)\r\n\tat udigodriver.steps.LoginStep.i_open_the_home_page_page(LoginStep.java:13)\r\n\tat ✽.Given I open the Home page page https://admin-test.witzmobility.com/home(alogin.feature:5)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "LoginStep.i_verify_the_login_screen_in_udigo()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "superAdmin@taxiApp.com",
      "offset": 13
    },
    {
      "val": "Password",
      "offset": 47
    }
  ],
  "location": "LoginStep.i_type_email_password(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "LoginStep.i_check_im_not_a_robot()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "LoginStep.i_click_the_login_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 2358100,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 15,
  "name": "Open the Driver Management page successfully",
  "description": "",
  "id": "in-oder-to-the-open-home-page-successfully;open-the-driver-management-page-successfully",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "comments": [
    {
      "line": 16,
      "value": "#      Given I open the Driver Management page \u003cwebsite\u003e"
    }
  ],
  "line": 17,
  "name": "I verify the Driver Management screen in udigo",
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click Driver Management button",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I click Driver Approval button",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "I click the Approve button",
  "keyword": "And "
});
formatter.examples({
  "line": 22,
  "name": "",
  "description": "",
  "id": "in-oder-to-the-open-home-page-successfully;open-the-driver-management-page-successfully;",
  "rows": [
    {
      "cells": [
        "website"
      ],
      "line": 23,
      "id": "in-oder-to-the-open-home-page-successfully;open-the-driver-management-page-successfully;;1"
    },
    {
      "cells": [
        "https://admin-test.witzmobility.com/driver"
      ],
      "line": 24,
      "id": "in-oder-to-the-open-home-page-successfully;open-the-driver-management-page-successfully;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 113100,
  "status": "passed"
});
formatter.scenario({
  "line": 24,
  "name": "Open the Driver Management page successfully",
  "description": "",
  "id": "in-oder-to-the-open-home-page-successfully;open-the-driver-management-page-successfully;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@ResetWeb"
    },
    {
      "line": 1,
      "name": "@Alogin"
    }
  ]
});
formatter.step({
  "comments": [
    {
      "line": 16,
      "value": "#      Given I open the Driver Management page \u003cwebsite\u003e"
    }
  ],
  "line": 17,
  "name": "I verify the Driver Management screen in udigo",
  "keyword": "Then "
});
formatter.step({
  "line": 18,
  "name": "I click Driver Management button",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I click Driver Approval button",
  "keyword": "When "
});
formatter.step({
  "line": 20,
  "name": "I click the Approve button",
  "keyword": "And "
});
formatter.match({
  "location": "DriverManagementStep.i_verify_the_driver_management_screen_in_udigo()"
});
formatter.result({
  "duration": 7060306500,
  "error_message": "org.openqa.selenium.WebDriverException: Failed to convert data to an object\nBuild info: version: \u00273.12.0\u0027, revision: \u00277c6e0b3\u0027, time: \u00272018-05-08T14:04:26.12Z\u0027\nSystem info: host: \u0027DESKTOP-2RQEVQT\u0027, ip: \u0027169.254.248.116\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_181\u0027\nDriver info: org.openqa.selenium.firefox.FirefoxDriver\nCapabilities {acceptInsecureCerts: true, browserName: firefox, browserVersion: 62.0.3, javascriptEnabled: true, moz:accessibilityChecks: false, moz:headless: false, moz:processID: 6692, moz:profile: C:\\Users\\Cao Nguyen\\AppData..., moz:useNonSpecCompliantPointerOrigin: false, moz:webdriverClick: true, pageLoadStrategy: normal, platform: XP, platformName: XP, platformVersion: 10.0, rotatable: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}}\nSession ID: 204afc15-e750-496b-8d94-83f2591937dc\n*** Element info: {Using\u003dxpath, value\u003d//aside/div/ul/li[2]/a}\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:187)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:122)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:543)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:317)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElementByXPath(RemoteWebDriver.java:419)\r\n\tat org.openqa.selenium.By$ByXPath.findElement(By.java:353)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.findElement(RemoteWebDriver.java:309)\r\n\tat org.openqa.selenium.support.pagefactory.DefaultElementLocator.findElement(DefaultElementLocator.java:69)\r\n\tat org.openqa.selenium.support.pagefactory.internal.LocatingElementHandler.invoke(LocatingElementHandler.java:38)\r\n\tat com.sun.proxy.$Proxy17.isDisplayed(Unknown Source)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.elementIfVisible(ExpectedConditions.java:315)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.access$100(ExpectedConditions.java:44)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$10.apply(ExpectedConditions.java:301)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$10.apply(ExpectedConditions.java:298)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:248)\r\n\tat udigodriver.screens.driverManagement.DriverManagementScreen.verifyDriverManagement(DriverManagementScreen.java:47)\r\n\tat udigodriver.steps.DriverManagementStep.i_verify_the_driver_management_screen_in_udigo(DriverManagementStep.java:30)\r\n\tat ✽.Then I verify the Driver Management screen in udigo(alogin.feature:17)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "DriverManagementStep.i_click_driver_management_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "DriverManagementStep.i_click_driver_approval_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "DriverManagementStep.i_click_the_approve_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 97300,
  "status": "passed"
});
formatter.uri("driver.feature");
formatter.feature({
  "line": 2,
  "name": "In oder to open the Driver Management page successfully",
  "description": "",
  "id": "in-oder-to-open-the-driver-management-page-successfully",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@ResetWeb"
    },
    {
      "line": 1,
      "name": "@Driver"
    }
  ]
});
formatter.scenarioOutline({
  "line": 4,
  "name": "Open the Driver Management page successfully",
  "description": "",
  "id": "in-oder-to-open-the-driver-management-page-successfully;open-the-driver-management-page-successfully",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 5,
  "name": "I open the Driver Management page \u003cwebsite\u003e",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I verify the Driver Management screen in udigo",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "I click Driver Management button",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "I click Driver Approval button",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I click the Approve button",
  "keyword": "And "
});
formatter.examples({
  "line": 11,
  "name": "",
  "description": "",
  "id": "in-oder-to-open-the-driver-management-page-successfully;open-the-driver-management-page-successfully;",
  "rows": [
    {
      "cells": [
        "website"
      ],
      "line": 12,
      "id": "in-oder-to-open-the-driver-management-page-successfully;open-the-driver-management-page-successfully;;1"
    },
    {
      "cells": [
        "https://admin-test.witzmobility.com/driver"
      ],
      "line": 13,
      "id": "in-oder-to-open-the-driver-management-page-successfully;open-the-driver-management-page-successfully;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 251900,
  "status": "passed"
});
formatter.scenario({
  "line": 13,
  "name": "Open the Driver Management page successfully",
  "description": "",
  "id": "in-oder-to-open-the-driver-management-page-successfully;open-the-driver-management-page-successfully;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 1,
      "name": "@ResetWeb"
    },
    {
      "line": 1,
      "name": "@Driver"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I open the Driver Management page https://admin-test.witzmobility.com/driver",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I verify the Driver Management screen in udigo",
  "keyword": "Then "
});
formatter.step({
  "line": 7,
  "name": "I click Driver Management button",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "I click Driver Approval button",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I click the Approve button",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "https://admin-test.witzmobility.com/driver",
      "offset": 34
    }
  ],
  "location": "DriverManagementStep.i_open_the_driver_management_page(String)"
});
formatter.result({
  "duration": 1847259800,
  "error_message": "org.openqa.selenium.WebDriverException: Failed to decode response from marionette\nBuild info: version: \u00273.12.0\u0027, revision: \u00277c6e0b3\u0027, time: \u00272018-05-08T14:04:26.12Z\u0027\nSystem info: host: \u0027DESKTOP-2RQEVQT\u0027, ip: \u0027169.254.248.116\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u00271.8.0_181\u0027\nDriver info: org.openqa.selenium.firefox.FirefoxDriver\nCapabilities {acceptInsecureCerts: true, browserName: firefox, browserVersion: 62.0.3, javascriptEnabled: true, moz:accessibilityChecks: false, moz:headless: false, moz:processID: 6692, moz:profile: C:\\Users\\Cao Nguyen\\AppData..., moz:useNonSpecCompliantPointerOrigin: false, moz:webdriverClick: true, pageLoadStrategy: normal, platform: XP, platformName: XP, platformVersion: 10.0, rotatable: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}}\nSession ID: 204afc15-e750-496b-8d94-83f2591937dc\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:187)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:122)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:543)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.get(RemoteWebDriver.java:271)\r\n\tat udigodriver.screens.driverManagement.DriverManagementScreen.openWebsite(DriverManagementScreen.java:37)\r\n\tat udigodriver.steps.DriverManagementStep.i_open_the_driver_management_page(DriverManagementStep.java:15)\r\n\tat ✽.Given I open the Driver Management page https://admin-test.witzmobility.com/driver(driver.feature:5)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "DriverManagementStep.i_verify_the_driver_management_screen_in_udigo()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "DriverManagementStep.i_click_driver_management_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "DriverManagementStep.i_click_driver_approval_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "DriverManagementStep.i_click_the_approve_button()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 70500,
  "status": "passed"
});
});