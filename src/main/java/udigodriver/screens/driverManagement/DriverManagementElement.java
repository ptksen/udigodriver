package udigodriver.screens.driverManagement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import udigodriver.core.Driver;

import java.util.List;

/**
 * Created by Cao Nguyen on 10/10/2018.
 */
public class DriverManagementElement {
    @FindBy(xpath = "//aside/div/ul/li[2]/a")
    protected WebElement btnDriverManagement;

    @FindBy(xpath = "//li[2]/div/ul/li[1]/a/span/span")
    protected WebElement bntDriverApproval;

    public WebElement getBtnApprove(String phoneNo) {
        List<WebElement> listTr = Driver.getDriver().findElement(By.xpath("//div[2]/div[2]")).
                findElements(By.className("pannelList list-group-item"));
        for (WebElement elementTr : listTr) {
            List<WebElement> listTd1 = elementTr.findElement(By.tagName("a")).findElements(By.tagName("span"));
            if (listTd1.get(0).getText().equals(phoneNo)) {
                WebElement btn = elementTr.findElement(By.tagName("span")).findElement(By.className("pannelListButtonGreen btn btn-default"));
                return btn;
            }
        }
        System.out.println("Can't find the information");
        return null;
    }

    public WebElement getBtnReject(String phoneNo) {
        List<WebElement> listTr = Driver.getDriver().findElement(By.xpath("//div[2]/div[2]")).
                findElements(By.className("pannelList list-group-item"));
        for (WebElement elementTr : listTr) {
            List<WebElement> listTd2 = elementTr.findElement(By.tagName("span")).findElements(By.tagName("button"));
            if (listTd2.get(0).getText().equals(phoneNo)) {
                WebElement btn = elementTr.findElement(By.tagName("span")).findElement(By.className("pannelListButtonRed btn btn-default"));
                return btn;
            }
        }
        System.out.println("Can't find the information");
        return null;
    }

    @FindBy(xpath = "//li[2]/div/ul/li[2]/a/span/span")
    protected WebElement btnDriversList;


}
