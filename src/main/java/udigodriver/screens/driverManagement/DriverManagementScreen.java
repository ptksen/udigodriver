package udigodriver.screens.driverManagement;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import udigodriver.core.Driver;

/**
 * Created by Cao Nguyen on 10/10/2018.
 */
public class DriverManagementScreen extends DriverManagementElement {
    public static WebDriver driver;
    private static DriverManagementScreen INSTANCE;
    private static WebDriverWait wait;

    private DriverManagementScreen() {
        initDriverPage();
    }

    public static DriverManagementScreen getInstance() {
        if (INSTANCE == null || (driver != null && Driver.getDriver() != driver)) {
            INSTANCE = new DriverManagementScreen();
        }
        return INSTANCE;
    }

    private void initDriverPage() {
        Driver.openDriver();
        driver = Driver.getDriver();
        PageFactory.initElements(driver, this);
        wait = Driver.getWait();
    }

    public void openWebsite(String website) {
        driver.get(website);
    }

    public void closeWebsite() {
        driver.quit();
        Driver.driver = null;

    }

    public void verifyDriverManagement(){
        wait.until(ExpectedConditions.visibilityOf(btnDriverManagement));
        Assert.assertNotNull(btnDriverManagement.isDisplayed());
    }

    public void clickDriverManagement(){
        wait.until(ExpectedConditions.visibilityOf(btnDriverManagement));
        btnDriverManagement.click();
    }

    public void clickDriverApproval(){
        wait.until(ExpectedConditions.visibilityOf(btnDriverManagement));
        bntDriverApproval.click();
    }

    public void clickBtnApprove() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
        if (this.getBtnApprove("84932532478") != null) {
            this.getBtnApprove("84932532478").click();
        }
    }

    public void clickDriversList(){
        wait.until(ExpectedConditions.visibilityOf(btnDriversList));
        btnDriversList.click();
    }
}
