package udigodriver.screens.login;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginElement {
    @FindBy(id = "email")
    protected WebElement txtEmail;

    @FindBy(id = "password")
    protected WebElement txtPassword;

//    @FindBy(xpath = "//*[@id=\"recaptcha-anchor\"]/div[5]")
//    protected WebElement chkrobot;

    @FindBy(xpath = "//button")
    protected WebElement btnLogin;

}


