package udigodriver.screens.login;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import udigodriver.core.Driver;

public class LoginScreen extends LoginElement {
    public static WebDriver driver;
    private static LoginScreen INSTANCE;
    private static WebDriverWait wait;

    private LoginScreen() {
        initDriverPage();
    }

    public static LoginScreen getInstance() {
        if (INSTANCE == null || (driver != null && Driver.getDriver() != driver)) {
            INSTANCE = new LoginScreen();
        }
        return INSTANCE;
    }

    private void initDriverPage() {
        Driver.openDriver();
        driver = Driver.getDriver();
        PageFactory.initElements(driver, this);
        wait = Driver.getWait();
    }

    public void openWebsite(String website) {
        driver.get(website);
    }


    public void closeWebsite() {
        driver.quit();
        Driver.driver = null;

    }

    public void verifylogin() {
        wait.until(ExpectedConditions.visibilityOf(txtEmail));
        Assert.assertNotNull(txtEmail.isDisplayed());
    }

    public void enterlogin(String emailInput, String passwordInput) {
        wait.until(ExpectedConditions.visibilityOf(txtEmail));
        txtEmail.sendKeys(emailInput);
        wait.until(ExpectedConditions.visibilityOf(txtPassword));
        txtPassword.sendKeys(passwordInput);
        try {
            Thread.sleep(60000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void clickChkRobot() {
        try {
            Thread.sleep(120000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void clickLogin() {
        wait.until(ExpectedConditions.visibilityOf(btnLogin));
        btnLogin.click();
    }
}
