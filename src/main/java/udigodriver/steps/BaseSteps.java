package udigodriver.steps;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import udigodriver.core.Driver;
import udigodriver.screens.driverManagement.DriverManagementScreen;
import udigodriver.screens.login.LoginScreen;

import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BaseSteps {
    HashMap<String, Object> scenarioContainer;

    BaseSteps() {
        scenarioContainer = new HashMap<String, Object>();
    }

    public static void tap(WebElement element) {
        Driver.getWait().until(ExpectedConditions.elementToBeClickable(element));
        element.click();
    }

    public static void enter(WebElement element, String value) {
        Driver.getWait().until(ExpectedConditions.visibilityOf(element));
        element.clear();
        element.sendKeys(value);
    }

    public static void selectFromListByIndex(List<WebElement> listElement, int index) {
        assertTrue(listElement.size() > 0);
        listElement.get(index).click();
    }

    public static void isDisplayed(WebElement element) {
        Driver.getWait().until((ExpectedConditions.visibilityOf(element)));
        assertTrue(element.isDisplayed());
    }

    public static void isNotDisplayed(List<WebElement> listElement) {
        assertEquals(0, listElement.size());
    }


    public static void verifyTextEquals(WebElement element, String expectedText) {
        Assert.assertEquals(expectedText, element.getText());
    }

    public static void verifyTextContains(WebElement element, String containString) {
        Assert.assertTrue(element.getText().contains(containString));
    }

    void openApp() {
        Driver.openDriver();
    }

    LoginScreen getLoginScreen(){
        return LoginScreen.getInstance();
    }

    DriverManagementScreen getDriverManagementScreen(){
        return DriverManagementScreen.getInstance();
    }
}
