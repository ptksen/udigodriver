package udigodriver.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Created by Cao Nguyen on 10/10/2018.
 */
public class DriverManagementStep extends BaseSteps {

    @Given("^I open the Driver Management page (.+)$")
    public void i_open_the_driver_management_page(String website) {
        getDriverManagementScreen().openWebsite(website);
    }

    @When("^I click Driver Management button$")
    public void i_click_driver_management_button() {
        getDriverManagementScreen().clickDriverManagement();
    }

    @When("^I click Driver Approval button$")
    public void i_click_driver_approval_button() {
        getDriverManagementScreen().clickDriverApproval();
    }

    @Then("^I verify the Driver Management screen in udigo$")
    public void i_verify_the_driver_management_screen_in_udigo() {
        getDriverManagementScreen().verifyDriverManagement();
    }

    @And("^I click the Approve button$")
    public void i_click_the_approve_button() {
        getDriverManagementScreen().clickDriverApproval();
    }

}
