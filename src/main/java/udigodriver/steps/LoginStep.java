package udigodriver.steps;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Created by Cao Nguyen on 10/7/2018.
 */
public class LoginStep extends BaseSteps {
    @Given("^I open the Home page page (.+)$")
    public void i_open_the_home_page_page(String website) throws Throwable {
        getLoginScreen().openWebsite(website);
    }
    @Then("^I verify the login screen in udigo$")
    public void i_verify_the_login_screen_in_udigo() throws Throwable {
       getLoginScreen().verifylogin();
    }
    @When("^I type email (.+) , password (.+)$")
    public void i_type_email_password(String email, String password) throws Throwable {
        getLoginScreen().enterlogin(email, password);
    }
    @And("^I check i'm not a robot$")
    public void i_check_im_not_a_robot() throws Throwable {
        getLoginScreen().clickChkRobot();
    }
    @Then("^I click the login button$")
    public void i_click_the_login_button() throws Throwable {
        getLoginScreen().clickLogin();
    }

}
