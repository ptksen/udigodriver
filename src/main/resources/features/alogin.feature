@ResetWeb @Alogin
  Feature: In oder to the Open Home page successfully

    Scenario Outline:  Open the Home page successfully
      Given I open the Home page page <website>
      Then I verify the login screen in udigo
      When I type email <email> , password <password>
      And I check i'm not a robot
      Then I click the login button

      Examples:
        | website                                  | email | password |
        | https://admin-test.witzmobility.com/home | superAdmin@taxiApp.com |  Password   |

    Scenario Outline:  Open the Driver Management page successfully
#      Given I open the Driver Management page <website>
      Then I verify the Driver Management screen in udigo
      When I click Driver Management button
      When I click Driver Approval button
      And I click the Approve button

      Examples:
        | website                                  |
        | https://admin-test.witzmobility.com/driver |

