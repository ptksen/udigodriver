@ResetWeb @Driver
  Feature: In oder to open the Driver Management page successfully

    Scenario Outline:  Open the Driver Management page successfully
      Given I open the Driver Management page <website>
      Then I verify the Driver Management screen in udigo
      When I click Driver Management button
      When I click Driver Approval button
      And I click the Approve button

      Examples:
        | website                                  |
        | https://admin-test.witzmobility.com/driver |

